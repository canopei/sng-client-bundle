<?php

namespace Sng\Symfony;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SngClientBundle extends Bundle
{
    const VERSION = '0.0.1';
}
