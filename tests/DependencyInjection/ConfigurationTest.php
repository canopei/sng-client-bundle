<?php
namespace Sng\Symfony\DependencyInjection;

use AppKernel;
use Symfony\Component\Filesystem\Filesystem;
use PHPUnit\Framework\TestCase;

class ConfigurationTest extends TestCase
{
    public function setUp()
    {
        (new Filesystem)
            ->remove(implode(DIRECTORY_SEPARATOR, [
                dirname(__DIR__),
                'fixtures',
                'cache',
                'test',
            ]));
    }

    /**
     * @dataProvider formatProvider
     */
    public function testContainerIsCompiling($format)
    {
        $kernel = new AppKernel('test', true, $format);
        $kernel->boot();

        $this->assertTrue($kernel->getContainer()->has('sng_client'));
    }

    public function formatProvider()
    {
        return [
            ['yml'],
            ['php'],
            ['xml'],
        ];
    }
}