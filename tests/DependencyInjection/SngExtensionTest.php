<?php

namespace Aws\Symfony\DependencyInjection;

use AppKernel;
use PHPUnit\Framework\TestCase;

class AwsExtensionTest extends TestCase
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function setUp()
    {
        $kernel = new AppKernel('test', true);
        $kernel->boot();
        $this->container = $kernel->getContainer();
    }

    public function testSngClientExists()
    {
        $this->assertTrue($this->container->has('sng_client'));
    }
}