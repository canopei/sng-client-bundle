<?php

use Symfony\Component\DependencyInjection\Reference;

$container->loadFromExtension('framework', [
    'secret' =>  'Rosebud was the name of his sled.',
]);

$container->loadFromExtension('sng', [
    'version' => 'latest'
]);